import httplib
import sys

#get http server ip
http_server = ('194.29.175.240', 1154)
#create a connection
conn = httplib.HTTPConnection('194.29.175.240', 1154)

while 1:
    cmd = raw_input('input command (ex. GET index.html): ')
    cmd = cmd.split()

    if cmd[0] == 'exit':
        break
    
    #request command to server
    conn.request(cmd[0], cmd[1])

    #get response from server
    rsp = conn.getresponse()
    
    #print server response and data
    print(rsp.status, rsp.reason)
    data_received = rsp.read()
    print(data_received)
    
conn.close()