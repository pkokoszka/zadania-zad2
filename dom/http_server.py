# -*- coding: utf-8 -*-

import socket
import sys
import logging
import logging.config
import email.utils
import os.path
from mimetypes import MimeTypes
from os import listdir
from os.path import isfile, join
import urllib



def createHtmlHeaders(isExist, isValid, url="", **additional_errors):

    mime_type = ''
    if(isExist):
        response_proto = 'HTTP/1.1'
        response_status = '200'
        response_status_text = 'OK \r\n'
        mime = MimeTypes()
        url = urllib.pathname2url('web' + url)
        mime_type = mime.guess_type(url)[0]

    else:
        response_proto = 'HTTP/1.1'
        response_status = '404'
        response_status_text = 'File not found \r\n'
        mime_type = "text/html"

    if(not isValid):
        response_proto = 'HTTP/1.1'
        response_status = '405'
        response_status_text = 'Method Not Allowed \r\n'
        mime_type = "text/html"

    if("error500" in additional_errors):
        response_proto = 'HTTP/1.1'
        response_status = '500'
        response_status_text = 'Internal Server Error \r\n'
        mime_type = "text/html"



    response_headers = {
        'content-type': mime_type, #'text/html'
        #'Vary':"Accept-Encoding"
        #'GMT Date': email.utils.formatdate(),
        #'content-length': len, #len(html) #len if isExist else 14
        #'connection': 'close',
    }


    response_headers_raw = ''.join('%s: %s\r\n' % (k, v) for k, v in \
                                                    response_headers.iteritems())

    toReturn = '%s %s %s' % (response_proto, response_status, \
                                                        response_status_text)
    return response_headers_raw, toReturn

def send500Error(connection):
    request = connection.recv(2048)
    response_headers_raw, toSend = createHtmlHeaders(True, True, error500="yes")
    sendToClient(connection, toSend, response_headers_raw, "web/500.html")


def sendToClient(connection, toSend, response_headers_raw, url, indexOf=""):
    connection.send(toSend)
    connection.send(response_headers_raw)
    connection.send('\r\n')

    if(len(indexOf) == 0):
        fopen = open(url, 'rb')
        connection.send(fopen.read())
    else:
        connection.send(indexOf)

    #connection.wfile.write(fopen.read())


def createIndexOf(url, elementsInDir):
    toReturn = "<!DOCTYPE html><html><head><title>Index of "+url+"</title></head><body><h1>Index of "+url+"</h1><table><tr><th>Name</th><th>Type</th></tr><tr><th colspan='5'><hr></th></tr>"

    if(url == "/"):
        url = ''


    for f in elementsInDir:
        if(os.path.isfile("web" + url + "/"+ f)):
            toReturn = toReturn + "<tr><td><a href="+ url + "/" + f+">"+f+"</a></td><td>FILE</td></tr>"
        if(os.path.isdir("web" + url + "/"+ f)):
            toReturn = toReturn + "<tr><td><a href="+ url + "/" + f+">"+f+"</a></td><td>DIR</td></tr>"

    toReturn = toReturn + "<tr><th colspan='5'><hr></th></tr></table></body></html>"
    return toReturn

def handle_client(connection, logger):
    """Obsługa konwersacji HTTP z pojedynczym klientem

    connection: socket klienta
    html:       wczytana strona html do zwrócenia klientowi
    logger:     mechanizm do logowania wiadomości
    """
    # Odebranie żądania
    # TODO: poprawnie obsłużyć żądanie dowolnego rozmiaru
    request = connection.recv(2048)
    logger.info(u'odebrano: "{0}"'.format(request))


    # Wysłanie zawartości strony

    czesciZapytan = request.split(' ')
    isValid = True
    isExist = False
    isExistDir = False
    indexOf=""

    if(os.path.isfile("web/" + czesciZapytan[1])):
        isExist = True
    else:
        isExist = False

    if(os.path.isdir("web/" + czesciZapytan[1])):
        isExistDir = True
        elementsInDir = [ f  for f  in listdir("web/" + czesciZapytan[1])] #if isfile(join("web/" + czesciZapytan[1],f))
        indexOf = createIndexOf(czesciZapytan[1], elementsInDir)


    if(czesciZapytan[0] != "GET"):
        isValid = False
        if(czesciZapytan[0] == "HTTP/1.1" and not "GET" in czesciZapytan[1]):
            isValid = True


    if(isExist):
        url = 'web/' + czesciZapytan[1]
    else:
        url = 'web/404.html'

    response_headers_raw, toSend = createHtmlHeaders(isExist, isValid, url=czesciZapytan[1])
    sendToClient(connection, toSend, response_headers_raw, url, indexOf=indexOf)

    logger.info(u'wysyłano odpowiedź')


def http_serve(server_socket, logger):
    """Obsługa połączeń HTTP

    server_socket:  socket serwera
    html:           wczytana strona html do zwrócenia klientowi
    logger:         mechanizm do logowania wiadomości
    """
    while True:
        # Czekanie na połączenie
        connection, client_address = server_socket.accept()
        logger.info(u'połączono z {0}:{1}'.format(*client_address))

        try:
            handle_client(connection, logger)
        except:
            send500Error(connection)
        finally:
            # Zamknięcie połączenia
            connection.close()


def server(logger):
    """Server HTTP

    logger: mechanizm do logowania wiadomości
    """
    # Tworzenie gniazda TCP/IP
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # Ustawienie ponownego użycia tego samego gniazda
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    # Powiązanie gniazda z adresem
    server_address = ('localhost', 4689)  # TODO: zmienić port!
    server_socket.bind(server_address)
    logger.info(u'uruchamiam server na {0}:{1}'.format(*server_address))

    # Nasłuchiwanie przychodzących połączeń
    server_socket.listen(1)



    try:
        http_serve(server_socket, logger)
    finally:
        server_socket.close()


if __name__ == '__main__':
    logging.config.fileConfig('logging.conf')
    logger = logging.getLogger('http_server')
    server(logger)
    sys.exit(0)